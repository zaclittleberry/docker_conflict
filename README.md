# Docker Conflict

The point of this repo is to demonstrate a situation in which two docker configurations in two different repos can end up conflicting with each other preventing them from both running at the same time.

## How to test

Open a terminal in the `proj_a` directory and run `docker-compose up -d`. Its command in `tail -f /dev/null` which should just hold the container open, and it's mapped to the host port `8083`. Run `docker-compose ps` to ensure it's running.

Open a terminal in the `proj_b` directory and run `docker-compose up -d`. It *should* be mapped to `8084`, but if you run `docker-compose ps` you will likely see that it shows `8083`, and your `up` command probably said "Recreating" instead of "Creating" if it had started independent containers.

If in `proj_b` you run `docker-compose stop`, it will shut down the containers started from `proj_a`.

## What this means

All this to say this is probably not the expected behavior by most users and has unintended consequences, such as not being able to run both repos on the same host, commands run in one directory and intended for one repo accidentally being run against another container.

## How to fix/avoid

More on this soon...
